package com.googlesheettemplate.controller;

import com.googlesheettemplate.service.GoogleSheetService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;
import java.util.Properties;

@RestController
@RequestMapping("/test_google_sheet")
public class GoogleSheetTemplateController {

    @Value("${gsheet.sheetId}")
    private String sheetId;

    @Value("${gsheet.range}")
    private String range;

    private final GoogleSheetService googleSheetService;

    public GoogleSheetTemplateController(GoogleSheetService googleSheetService) {
        this.googleSheetService = googleSheetService;
    }

    @GetMapping
    public void testGoogleSheet() {
        try {
            List<Object> values = googleSheetService.readFromSpreadsheet(sheetId, range);
            values.forEach(System.out::println);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
