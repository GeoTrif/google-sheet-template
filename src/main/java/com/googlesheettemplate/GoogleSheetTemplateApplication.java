package com.googlesheettemplate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GoogleSheetTemplateApplication {

    public static void main(String[] args) {
        SpringApplication.run(GoogleSheetTemplateApplication.class, args);
    }

}
